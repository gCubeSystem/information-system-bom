This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for Information System Bom


## [v2.0.0-SNAPSHOT] [r5.0.0] -

- Switched JSON management to gcube-jackson [#19116]

## [v1.5.0] [r4.21.0] - 2020-03-30

- Upgraded OrientDB® from version 2.2.X to 3.0.X [#13180]


## [v1.4.0] [r4.13.0] - 2018-11-20

- Cleaned bom


## [v1.3.0] [r4.9.0] - 2017-12-20

- Updated OrientDB® Version to 2.2.30


## [v1.2.0] [r4.5.0] - 2017-06-07

- Updated OrientDB® Version to 2.2.18
- Removed unneeded dependencies declaration (Thinkerpop™ Frames)


## [v1.1.0] [r4.3.0] - 2017-03-16

- Updated OrientDB® Version to 2.2.17


## [v1.0.0] [r4.2.0] - 2016-12-16

- First Release

